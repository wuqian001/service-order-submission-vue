import { Component } from 'react'
import './app.scss'
import { Provider } from 'react-redux'
import { createStore, combineReducers } from 'redux';



/* 自定义图标 */
import './icon.scss'
import Taro from '@tarojs/taro'

import configStore from './store'
// 如果需要在 h5 环境中开启 React Devtools
// 取消以下注释：
// if (process.env.NODE_ENV !== 'production' && process.env.TARO_ENV === 'h5')  {
//   require('nerv-devtools')
// }

console.log(process.env.TARO_ENV)
if (process.env.TARO_ENV == 'qywx')  {
  console.log(Taro.getEnv())
  console.log("===12123123123123123");
}
const store = configStore();

class App extends Component {

  componentDidMount () {
    //将redux状态挂载到 Taro 对象上，方便使用
    Taro.$store = store;
    Taro.getSetting({
      success: function (res) {
        console.log(res)
      }
    })
  }

  // this.props.children 是将要会渲染的页面
  render () {
    return (
      <Provider store={store}>
        {this.props.children}
      </Provider>
    )
  }
}

export default App