export default {
  pages: [
    'pages/index/index',
    'pages/index/order-detail',
    'pages/index/order-detail-edit'
  ],
  window: {
    backgroundTextStyle: 'light',
    navigationBarBackgroundColor: '#fff',
    navigationBarTitleText: 'WeChat',
    navigationBarTextStyle: 'black'
  }
}
