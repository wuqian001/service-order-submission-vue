import { Component } from 'react'
import { View } from '@tarojs/components'
import { AtButton, AtInput } from 'taro-ui'

import "taro-ui/dist/style/components/button.scss";
import "taro-ui/dist/style/components/loading.scss";
import './comment.scss'

import {addComment} from "../api"

class WqCommentBar extends Component{
    constructor () {
        super(...arguments)
        this.state = {
            loading:false,
            value:"",
            oid:"0"
        }
    }
    componentDidMount () {
    }
    async addComment(){
        const { id } = this.props
        let param = {};
        param.oid = id;
        this.setState({
            loading:true
        })
        param.content = this.state.value;
        await addComment(param)
        this.props.callback();
        this.setState({
            loading:false,
            value:""
        })
    }
    inputText(v){
        this.setState({
            value:v
        })
    }
    render (){
        return (
            <View className='at-row commentsInputLayout' style="border: 1px solid #ccc;padding:1px">
                <View className='at-col'>
                    <AtInput style="background: #fff;" value={this.state.value} onChange={this.inputText.bind(this)} type="text" placeholder="请输入评论内容" />
                </View>
                <View className='at-col at-col-1 at-col--auto'>
                    <AtButton loading={this.state.loading} onClick={this.addComment.bind(this)} className="commentsBtn" type='primary'>评论</AtButton>
                </View>
            </View>
        );
    }
}


export { WqCommentBar }