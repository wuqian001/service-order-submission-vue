import { Component } from 'react'
import { View } from '@tarojs/components'

import './index.scss'

class Container extends Component{
    render (){
        return (
            <View className="containerD" style={this.props.style}>
            {this.props.children}
            </View>
        );
    }
}


export { Container }