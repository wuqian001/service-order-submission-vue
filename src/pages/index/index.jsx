import { Component } from 'react'
import { View, Text } from '@tarojs/components'

import { AtTabs, AtTabsPane, AtSearchBar, AtFab, AtModal, AtIcon, AtModalContent, AtModalAction, AtButton } from "taro-ui"
import Taro from '@tarojs/taro'


import "taro-ui/dist/style/components/button.scss" // 按需引入
import "taro-ui/dist/style/components/tabs.scss"
import "taro-ui/dist/style/components/search-bar.scss"
import "taro-ui/dist/style/components/icon.scss"
import "taro-ui/dist/style/components/fab.scss"
import "taro-ui/dist/style/components/modal.scss"
import "taro-ui/dist/style/components/flex.scss"

import './index.scss'
import { OrderList } from './order-list'
import pageInit from '../../utils/pageInit'
import Auth from "../../utils/auth"
import SubsriceMessage from "../../utils/subsriceMessage"


@pageInit()
export default class Index extends Component {

  constructor () {
    super(...arguments)
    this.state = {
      current: 0,
      keywords: '',
      isOpened: false,
      tmplIds: []
    }
  }
  componentDidMount () {
    this.OrderList.setState({
      page: {
        pageNum:1,
        pageSize:10
      },
      list: [],
      where:{statue:[0,1]}
    })
    this.OrderList.getData();

    this.OrderListHistory.setState({
      page: {
        pageNum:1,
        pageSize:10
      },
      list: [],
      where:{statue:[2]}
    })
    this.OrderListHistory.getData();
    
    // this.isReSubscribeMessage();//处理订阅消息
  }
  onChange (value) {
    this.setState({
      keywords: value
    })
  }
  onAddClick () {
    Taro.navigateTo({
      url:"order-detail-edit"
    });
  }
  handleClick (value) {
    this.setState({
      current: value
    })
  }
  //执行搜索事件
  onActionSearch () {
    this.doActionData();
  }
  onPullDownRefresh () {
    this.doActionData()
    Taro.stopPullDownRefresh()
  }
  async doActionData (){
    if(this.state.current === 0){
      //下拉刷新
      let p_param = {
        page: {
          pageNum:1,
          pageSize:10
        },
        list: [],
        where:{statue:[0,1],keywords:this.state.keywords}
      }
      
      await this.OrderList.getData(p_param);
    }else{
      //下拉刷新
      let p_param = {
        page: {
          pageNum:1,
          pageSize:10
        },
        list: [],
        where:{statue:[2],keywords:this.state.keywords}
      }
      await this.OrderListHistory.getData(p_param);
    }
  }
  async onReachBottom () {
    if(this.state.current === 0){
      //上拉 加载更多
      await this.OrderList.getData();
    }else{
      await this.OrderListHistory.getData();
    }
  }

  reSubscribeMessage() {
     
    let tmplIds = [
      SubsriceMessage.HANDNOTICE,
      SubsriceMessage.WCNOTICE
    ]
    if(Auth.getSuperMan()) {
      tmplIds.push(SubsriceMessage.NEWNOTICE)
    }
    Taro.requestSubscribeMessage({
      tmplIds: tmplIds,
      success: function (res){
        console.log(res)
      }
    })
  }
  //挂载子组件 对象
  refOrderList = (node) => this.OrderList = node 
  refOrderListHistory = (node) => this.OrderListHistory = node 
  render () {
    const sysConfig = Taro.getSystemInfoSync();
 
    const tabList = [{ title: '进行中' }, { title: '完结工单' }]

    const message_tab = sysConfig?.environment=="wxwork"?"":(
      <View className='at-row at-row__justify--between' style="padding: 10px;background: #e4e4e4;font-size: 12px;">
        <View className='at-col at-col-5'><AtIcon value='bell' size='30' color='#F00'></AtIcon>接受不到工单状态改变提醒消息？</View>
        <View className='at-col at-col-3' onClick={this.reSubscribeMessage.bind(this)} style="color:#586bde">点此开启</View>
      </View>
    )
    return (
      <View>
        <AtSearchBar
          value={this.state.keywords}
          onChange={this.onChange.bind(this)}
          onActionClick={this.onActionSearch.bind(this)}
        />
        {message_tab}
        <AtTabs current={this.state.current} tabList={tabList} onClick={this.handleClick.bind(this)}>
          <AtTabsPane current={this.state.current} index={0} >
            <OrderList ref={this.refOrderList}></OrderList>
          </AtTabsPane>
          <AtTabsPane current={this.state.current} index={1}>
            <OrderList ref={this.refOrderListHistory}></OrderList>
          </AtTabsPane>
        </AtTabs>
        <View style="position: fixed;bottom: 16px;right: 16px;">
          <AtFab onClick={this.onAddClick.bind(this)}>
            <Text className='at-fab__icon at-icon at-icon-add'></Text>
          </AtFab>
        </View>
        <AtModal isOpened={this.state.isOpened}>
          <AtModalContent >
            <View style="display:flex; flex-direction: column;">
              <View style="min-height:120rpx;font-size: 14px;">接受通知消息?</View>
              <View style="flex:1">
              <AtButton type="secondary" size="small" onClick={this.reSubscribeMessage.bind(this)}>订阅</AtButton>
              </View>
            </View>
          </AtModalContent>
        </AtModal>
      </View>
    )
  }
}
