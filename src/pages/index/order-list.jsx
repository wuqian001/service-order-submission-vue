import { Component } from 'react'
import { AtList, AtListItem } from "taro-ui"
import { View, Image } from '@tarojs/components'
import Taro from '@tarojs/taro'
import "taro-ui/dist/style/components/list.scss"
import "taro-ui/dist/style/components/icon.scss"

import "taro-ui/dist/style/components/flex.scss"

import {getOrderlist} from "../api"

export class OrderList extends Component {
    
    constructor () {
        super(...arguments)
        this.state = {
            page: {
                pageNum:1,
                pageSize:10
            },
            where:{statue:[0,1]},
            list:[]
        }
    }
    
    async getData (p_param){
        let order = {orders:[{column:"create_time",asc:false}]}
        let do_param = {...this.state.page,...this.state.where,...order};
        if(p_param){
            this.setState(p_param)
            do_param = {...p_param?.page,...p_param?.where,...order}
        }
        
        let res = await getOrderlist(do_param);
        let page = this.state.page;
        let pageSize = this.state.page.pageSize;
        if( res.data.pages >= this.state.page.pageNum){
            //最大页数没有满
            // if(res.data.list.length >= pageSize){
                //满页数量  page +1
                page = {
                    pageNum: this.state.page.pageNum + 1,
                    pageSize: this.state.page.pageSize
                }
            // }
        }
        this.setState({
            page: page,
            list: this.state.list.concat(res.data.list)
        });
        
    }
    navigateTo (id) {
        Taro.navigateTo({
            url:"order-detail?id="+id
        });
        this.state.list[this.state.list.findIndex(item=>item.id===id)].isRead ++; 
        this.setState({
            list: this.state.list
        });
        
    }
    render () {

        let element;
        if(this.state.list.length>0){
            element = (<AtList>
            {this.state.list.map((item, index) => {
                let newIcon = {};
                if(item.isRead <= 0){
                    newIcon = {size: 18, color: '#FF4949', value: 'bookmark'};
                }       
                return (
                    <AtListItem iconInfo={newIcon} 
                    onClick={this.navigateTo.bind(this,item.id)} 
                    title={item.title} 
                    note={item.actualName+" "+item.createTime} arrow='right'
                    extraText={item.statue===0?'等待处理':item.statue===1?'正在处理':'处理完成'}
                    />
                )
            })}
            </AtList>);
        }else{
            element = (<View className='at-row at-row__justify--center'>
                <View className='at-col at-col-5'>
                    <Image className='at-col' style="height:180rpx;padding: 42rpx 42rpx 10rpx 42rpx;" mode="aspectFit" src={require("../img/want-empty.png")}></Image>
                    <View style="text-align: center;">没有更多数据！</View>
                </View>
            </View>);
        }
        return (<View>
                {element}
            </View>
        )
    }
}