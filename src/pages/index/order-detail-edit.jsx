import { Component } from 'react'
import { View, Text } from '@tarojs/components'
import { Picker } from '@tarojs/components'
import { ScrollView } from '@tarojs/components'
import { Container } from "../components"
import { AtImagePicker, AtTabBar, AtForm, AtInput, AtTextarea, AtButton } from 'taro-ui'
import Taro from '@tarojs/taro'
import { getCurrentInstance } from '@tarojs/taro'

import "taro-ui/dist/style/components/article.scss"
import "taro-ui/dist/style/components/flex.scss"

import "taro-ui/dist/style/components/card.scss"

import "taro-ui/dist/style/components/image-picker.scss"
import "taro-ui/dist/style/components/icon.scss"
import "taro-ui/dist/style/components/tab-bar.scss"
import "taro-ui/dist/style/components/badge.scss"
import "taro-ui/dist/style/components/form.scss"
import "taro-ui/dist/style/components/input.scss"
import "taro-ui/dist/style/components/list.scss"
import "taro-ui/dist/style/components/textarea.scss"
import "taro-ui/dist/style/components/button.scss"
import "taro-ui/dist/style/components/loading.scss"

import {addOrder, getOrderBase, getOrderInfo, updateOrder} from "../api"
import requestFileUpload from "../../utils/request-file-upload"

import "./order-detail.scss"

export default class OrderDetailEdit extends Component {
    constructor () {
        super(...arguments)
        this.state = {
            id:0,
            rangeType:[{label:'--请选择类型--',id:0}],
            departmentMap:{0:{label:'--请选择部门--',id:0}},
            workOrderTypeMap:{0:{label:'--请选择类型--',id:0}},
            type:0,
            rangeDepartment:[{label:'--请选择部门--',id:0}],
            departmentId:0,
            problemDetails:"",
            mobile:"",
            actualName:"",
            pics:[]
        }
    }
    async componentDidMount () { 
        
        //初始化
        //获取单据类型
        //获取部门数据
        let res = await getOrderBase(null);
        
        this.setState({
            departmentMap:{...this.state.departmentMap,...res.data.departmentMap},
            workOrderTypeMap:{...this.state.workOrderTypeMap,...res.data.workOrderTypeMap},
            rangeType: this.state.rangeType.concat(res.data.workOrderTypeList),
            rangeDepartment: this.state.rangeDepartment.concat(res.data.departmentList),
            mobile: res.data.employeeBaseVO.mobile,
            actualName: res.data.employeeBaseVO.actualName,
            departmentId: res.data.employeeBaseVO.departmentId
        })
        // console.log(this.state)
        //获取参数
        let params = getCurrentInstance().router.params;
        if(params.id > 0){
            let res = await getOrderInfo(params.id);
            this.setState({
                id: params.id,
                pics:res.data.pics===null?[]:res.data.pics,
                type: res.data.wot,
                departmentId: res.data.departmentId,
                actualName: res.data.actualName,
                mobile: res.data.mobile,
                problemDetails: res.data.problemDetails
            })
        }
    }
    onFail (mes) {  
        console.log(mes)
    }
    onImageClick (index, file) {
        const imgurls = this.state.files.map(item=>item.url);
        Taro.previewImage({
            current: file.url, // 当前显示图片的http链接
            urls: imgurls // 需要预览的图片http链接列表
          });
    }
    handleChange (name,obj,obj1,obj2) {
        
        let value = {};
        let v1;
        // console.log(obj)
        // console.log(name)
        if(typeof obj === 'string'||Array.isArray(obj)){
            if(name === "pics"){
                // console.log(obj)
                // console.log(name)
                // console.log(obj1);
                // console.log(obj2)
                let obj_n = [];
                obj.forEach(async item=>{
                    let pic_url;
                    if(item.file){
                        let res = await requestFileUpload( 
                            `/api/file/localUpload/1` ,
                            item.file.path
                        )
                        pic_url = res.url;
                    }else{
                        pic_url = item.url;
                    }
                    
                    obj_n.push({url: pic_url});
                    this.setState({pics:obj_n});
                })
                this.setState({pics:obj_n});
                return ;
            }
            else if(name === "type" || name === "departmentId"){
                // console.log(obj[obj1.detail.value]);
                v1 = obj[obj1.detail.value].id;
            }else{
                v1 = obj;
            }
            
            
        }else{
            v1 = obj.detail.value;
        }
        
        value[name] = v1;

        this.setState({
            ...value
        })

        // console.log(v1)
    }
    async onSubmit (event) {
        let param = {};
        param.wot = parseInt(this.state.workOrderTypeMap[this.state.type].id);
        if(!(param.wot>0)){
            Taro.showToast({
                title : '工单类型未选择' ,
                icon : 'none' ,
                mask : true
            })
            return ;
        }
        param.departmentId = parseInt(this.state.departmentMap[this.state.departmentId].id);
        if(!(param.departmentId>0)){
            Taro.showToast({
                title : '部门未选择' ,
                icon : 'none' ,
                mask : true
            })
            return ;
        }
        param.mobile = this.state.mobile;
        if(param.mobile<=0||!/^1(3|4|5|6|7|8|9)\d{9}$/.test(param.mobile)){
            Taro.showToast({
                title : '手机号未填或者手机号格式不正确' ,
                icon : 'none' ,
                mask : true
            })
            return ;
        }
        param.actualName = this.state.actualName
        if(param.actualName == ""){
            Taro.showToast({
                title : '请填写联系人姓名' ,
                icon : 'none' ,
                mask : true
            })
            return ;
        }
        param.problemDetails = this.state.problemDetails;
        if(param.problemDetails<=0){
            Taro.showToast({
                title : '问题描述未填' ,
                icon : 'none' ,
                mask : true
            })
            return ;
        }
        param.pics  = this.state.pics.map(item=>item.url);
        
        let res;
        if(this.state.id > 0){
            param.id = this.state.id;
            res = await updateOrder(param);
        }else{
            res = await addOrder(param);
        }
        Taro.redirectTo({
            url:"order-detail?id="+res.data
        })
        // console.log(this.state.value)
    }
    render() {
        return (
            <AtForm>
                <ScrollView
                className='scrollview'
                scrollY
                scrollWithAnimation
                >
                <View style="background:#f3f3f3">
                    <Container style="margin: 15px 10px 0;padding: 10px;">
                        
                        <View className="d-rows">
                            <View className='at-article__h3 panel__title' style="color:#999">工单类型选择<View style="display:contents;color:red"> *</View></View>
                            <View className="panel__content">
                            <Picker mode='selector' rangeKey={'label'} range={this.state.rangeType}
                                onChange={this.handleChange.bind(this, 'type',this.state.rangeType)}>
                                <View className='at-article__h3'>
                                {this.state.workOrderTypeMap[this.state.type].label}
                                </View>
                            </Picker>
                            </View>
                        </View>

                        <View className="d-rows">
                            <View className='at-article__h3 panel__title' style="color:#999">所在部门</View>
                            <View className="panel__content">
                            <Picker mode='selector' rangeKey={'label'}  range={this.state.rangeDepartment}
                                onChange={this.handleChange.bind(this, 'departmentId',this.state.rangeDepartment)}>
                                <View className='at-article__h3'>
                                {this.state.departmentMap[this.state.departmentId].label}
                                </View>
                            </Picker>
                            </View>
                        </View>
                        <View className="d-rows">
                            <View className='at-article__h3 panel__title' style="color:#999">联系人<View style="display:contents;color:red"> *</View></View>
                            <View className="panel__content">
                                <AtInput
                                name='actualName'
                                type="text"
                                placeholder='联系人'
                                value={this.state.actualName}
                                onChange={this.handleChange.bind(this, 'actualName')}
                                ></AtInput>
                            
                            </View>
                        </View>
                        <View className="d-rows">
                            <View className='at-article__h3 panel__title' style="color:#999">联系人电话<View style="display:contents;color:red"> *</View></View>
                            <View className="panel__content">
                                <AtInput
                                name='mobile'
                                type='phone'
                                placeholder='手机号码'
                                value={this.state.mobile}
                                onChange={this.handleChange.bind(this, 'mobile')}
                                ></AtInput>
                            
                            </View>
                        </View>
                        
                    </Container>
                    <Container style="margin: 15px 10px 0;padding: 10px;">
                        <View className="d-rows">
                            <View className='at-article__h3 panel__title' style="color:#999">问题描述<View style="display:contents;color:red"> *</View></View>
                            <AtTextarea
                                className="panel__content"
                                onChange={this.handleChange.bind(this, 'problemDetails')}
                                maxLength={200}
                                value={this.state.problemDetails}
                                placeholder='你的问题是...'
                            />
                        </View>
                        <View className="d-rows">
                            <View className='at-article__h3 panel__title'>图片</View>
                            <View className="panel__content">
                                <AtImagePicker
                                    mode='widthFix'
                                    files={this.state.pics}
                                    onFail={this.onFail.bind(this)}
                                    onChange={this.handleChange.bind(this,'pics')}
                                    onImageClick={this.onImageClick.bind(this)}
                                    />
                            </View>
                        </View>
                    </Container>
                    <AtButton type='primary' onClick={this.onSubmit.bind(this)}>提交</AtButton>
                    
                </View>
                </ScrollView>
            </AtForm>
        )
    }
}