import request from "../utils/request";

// 添加单据
const addOrder = (param) => request(
    `orders/add` ,
    param)
//修改
const updateOrder = (param) => request(
    `orders/update` ,
    param)

const getOrderlist = (param) => request(
    `orders/page/query`,
    param
)
//工单详情
const getOrderInfo = (id) => request(
    `orders/query_`+id,
    null,
    "GET"
)

//工单详情 基础数据
const getOrderBase = (param) => request(
    `orders/pageBase`,
    param,
    "GET"
)

//管理处理工单
const getOrderHand = (param) => request(
    `orders/handle`,
    param
)
const actionOrderPeroperHand = (param) =>request(
    `orders/changeOrderPeroperHand`,
    param
)
//点赞
const upOrderLike = (id) => request(
    `orders/like_`+id,
    null
)
//添加评论
const addComment = (param) => request(
    `orders/addComment`,
    param
)
//获取工单处理级别
const getGongdanImportanceLevel = ()=>request(
    `systemConfig/getListByGroup`,
    {group:"gongdan_importance_level_group"},
    'GET'
)
export {
    addOrder,
    updateOrder,
    getOrderlist,
    getOrderInfo,
    getOrderBase,
    getOrderHand,
    actionOrderPeroperHand,
    upOrderLike,
    addComment,
    getGongdanImportanceLevel
}