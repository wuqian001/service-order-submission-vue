import Taro from '@tarojs/taro'
import Auth from './auth'

//请求封装
function qyLogin (){

	Taro.showLoading({title: '加载中'});

	//返回promise
	return new Promise(resolve=>{
		wx.qy.login({
			success: function(res) {
				//添加片段
				resolve(res); 
			}
		});
	})
}

export default qyLogin;